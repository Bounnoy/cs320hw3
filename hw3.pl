build(L,X,Y) :- length(L,A), A =:= 2, [X,Y] = L.
build(L,X,Y) :- length(L,A), A =:= 3, [X,C,D] = L, build([C,D],J,K), make(J,K,Y).
build(L,X,Y) :- length(L,A), A =:= 3, [B,C,Y] = L, build([B,C],J,K), make(J,K,X).
build(L,X,Y) :- length(L,A), A =:= 4, [B,C,D,E] = L, build([B,C],J,K), make(J,K,X), build([D,E],M,N), make(M,N,Y).
build(L,X,Y) :- length(L,A), A =:= 4, [X,C,D,E] = L, build([C,D,E],J,K), make(J,K,Y).
build(L,X,Y) :- length(L,A), A =:= 4, [B,C,D,Y] = L, build([B,C,D],J,K), make(J,K,X).

op(A) :- integer(A).
op(A + B,Z) :- op(A), op(B), Z = A + B.
op(A + B,Z) :- op(A), op(B,C), Z = A + C.
op(A + B,Z) :- op(A,C), op(B), Z = C + B.
op(A + B,Z) :- op(A,C), op(B,D), Z = C + D.

op(A - B,Z) :- op(A), op(B), Z = A - B.
op(A - B,Z) :- op(A), op(B,C), Z = A - C.
op(A - B,Z) :- op(A,C), op(B), Z = C - B.
op(A - B,Z) :- op(A,C), op(B,D), Z = C - D.

op(A * B,Z) :- op(A), op(B), Z = A * B.
op(A * B,Z) :- op(A), op(B,C), Z = A * C.
op(A * B,Z) :- op(A,C), op(B), Z = C * B.
op(A * B,Z) :- op(A,C), op(B,D), Z = C * D.

op(A // B,Z) :- op(A), op(B), A > 0, B > 0, mod(A,B) =:= 0, Z = A // B.
op(A // B,Z) :- op(A), op(B,C), A > 0, C > 0, mod(A,C) =:= 0, Z = A // C.
op(A // B,Z) :- op(A,C), op(B), C > 0, B > 0, mod(C,B) =:= 0, Z = C // B.
op(A // B,Z) :- op(A,C), op(B,D), C > 0, D > 0, mod(C,D) =:= 0, Z = C // D.

eval(A,B,V) :- op(A), op(B), make(A,B,X), V is X.
eval(A,B,V) :- op(A), op(B,D), make(A,D,X), V is X.
eval(A,B,V) :- op(A,C), op(B), make(C,B,X), V is X.
eval(A,B,V) :- op(A,C), op(B,D), make(C,D,X), V is X.

print(A,B,C) :- C >= 0, C =< 9, make(A,B,Z), format('~w = ~w~n',[Z,C]).

make(A,B,Z) :- Z = A + B; Z = A - B; Z = A * B.
make(A,B,Z) :- A > 0, B > 0, mod(A,B) =:= 0, Z = A // B.

prep :- L = [10,10,10,10], build(L,X,Y), eval(X,Y,Z), print(X,Y,Z).
start :- findall(!,prep,_).
