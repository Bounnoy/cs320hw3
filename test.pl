:- begin_tests(test).

% (1 + 2) * 3
test(ev1) :-
  Z is (1 + 2) * 3,
  assertion(eval(1 + 2,3,Z)).

% 4 / (3 - 1) DIVISION OK
test(ev2) :-
  Z is 4 // (3 - 1),
  assertion(eval(4,3 - 1,Z)).

% 4 / (1 * 3) DIVISION NOT OK
test(ev3) :-
  Z is 4 // (1 * 3),
  assertion(eval(4,1 * 3,Z)).

:- end_tests(test).
